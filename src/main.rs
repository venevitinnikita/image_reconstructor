extern crate png;

use std::env;
use std::fs::File;

const IMAGE_WIDTH: usize = 512;
const IMAGE_HEIGHT: usize = 512;

const BLOCK_SIZE: usize = 16;

const THRESHOLD: i32 = 15;

#[derive(Copy, Clone)]
struct Pixel {
    r: u8,
    g: u8,
    b: u8,
}

impl Default for Pixel {
    fn default() -> Self {
        Self { r: 0, g: 0, b: 0 }
    }
}

impl Pixel {
    #[inline]
    fn same_component(a: u8, b: u8) -> bool {
        let a = a as i32;
        let b = b as i32;
        (a - b).abs() < THRESHOLD
    }

    fn same(&self, other: Self) -> bool {
        Self::same_component(self.r, other.r)
            && Self::same_component(self.g, other.g)
            && Self::same_component(self.b, other.b)
    }
}

struct Buffer {
    buf: Vec<u8>,
}

impl Buffer {
    fn read_from_png_file(path: &str) -> Self {
        let decoder = png::Decoder::new(File::open(path).unwrap());
        let (info, mut reader) = decoder.read_info().unwrap();

        assert_eq!(info.width, IMAGE_WIDTH as u32);
        assert_eq!(info.height, IMAGE_HEIGHT as u32);
        println!("{:?}", info);

        let mut buf = vec![0; info.buffer_size()];
        reader.next_frame(&mut buf).unwrap();
        Buffer { buf }
    }

    fn pixel(&self, idx: usize) -> Pixel {
        Pixel {
            r: self.buf[idx * 3],
            g: self.buf[idx * 3 + 1],
            b: self.buf[idx * 3 + 2],
        }
    }
}

struct Block {
    idx: usize,

    top: [Pixel; BLOCK_SIZE],
    bottom: [Pixel; BLOCK_SIZE],
    left: [Pixel; BLOCK_SIZE],
    right: [Pixel; BLOCK_SIZE],

    neighbour_top: Option<usize>,
    neighbour_bottom: Option<usize>,
    neighbour_left: Option<usize>,
    neighbour_right: Option<usize>,
}

impl Block {
    fn init_from_buffer(buf: &Buffer, idx: usize) -> Self {
        let mut top = [Pixel::default(); BLOCK_SIZE];
        let mut bottom = [Pixel::default(); BLOCK_SIZE];
        let mut left = [Pixel::default(); BLOCK_SIZE];
        let mut right = [Pixel::default(); BLOCK_SIZE];

        let blocks_in_row = IMAGE_WIDTH / BLOCK_SIZE;
        let row = idx / blocks_in_row;
        let col = idx % blocks_in_row;

        let block_start = row * IMAGE_WIDTH * BLOCK_SIZE + col * BLOCK_SIZE;

        let top_left_pixel = block_start;
        let top_right_pixel = block_start + BLOCK_SIZE - 1;
        let bottom_left_pixel = block_start + IMAGE_WIDTH * (BLOCK_SIZE - 1);

        for i in 0..BLOCK_SIZE {
            top[i] = buf.pixel(top_left_pixel + i);
            bottom[i] = buf.pixel(bottom_left_pixel + i);
            left[i] = buf.pixel(top_left_pixel + i * IMAGE_WIDTH);
            right[i] = buf.pixel(top_right_pixel + i * IMAGE_WIDTH);
        }

        Self {
            idx,

            top,
            bottom,
            left,
            right,

            neighbour_top: None,
            neighbour_bottom: None,
            neighbour_left: None,
            neighbour_right: None,
        }
    }

    fn same_edge(edge_a: &[Pixel], edge_b: &[Pixel]) -> bool {
        for i in 0..BLOCK_SIZE {
            if !edge_a[i].same(edge_b[i]) {
                return false;
            }
        }
        true
    }

    fn which_neighbour(&self, other: &Self) -> Neighbour {
        if self.neighbour_top == None && Self::same_edge(&self.top, &other.bottom) {
            Neighbour::Top
        } else if self.neighbour_bottom == None && Self::same_edge(&self.bottom, &other.top) {
            Neighbour::Bottom
        } else if self.neighbour_left == None && Self::same_edge(&self.left, &other.right) {
            Neighbour::Left
        } else if self.neighbour_right == None && Self::same_edge(&self.right, &other.left) {
            Neighbour::Right
        } else {
            Neighbour::None
        }
    }

    fn try_fit(&mut self, other: &mut Self) {
        match self.which_neighbour(&other) {
            Neighbour::Top => {
                self.neighbour_top = Some(other.idx);
                other.neighbour_bottom = Some(self.idx);
            }
            Neighbour::Bottom => {
                self.neighbour_bottom = Some(other.idx);
                other.neighbour_top = Some(self.idx);
            }
            Neighbour::Left => {
                self.neighbour_left = Some(other.idx);
                other.neighbour_right = Some(self.idx);
            }
            Neighbour::Right => {
                self.neighbour_right = Some(other.idx);
                other.neighbour_left = Some(self.idx);
            }
            Neighbour::None => (),
        }
    }

    fn has_all_neighbours(&self) -> bool {
        match (
            self.neighbour_top,
            self.neighbour_bottom,
            self.neighbour_left,
            self.neighbour_right,
        ) {
            (Some(_), Some(_), Some(_), Some(_)) => true,
            _ => false,
        }
    }
}

enum Neighbour {
    Top,
    Bottom,
    Left,
    Right,
    None,
}

fn main() {
    let args: Vec<String> = env::args().collect();

    let buf = Buffer::read_from_png_file("D:/test_data/data_train/16/0000.png");

    let blocks_count: usize = (IMAGE_WIDTH / BLOCK_SIZE) * (IMAGE_HEIGHT / BLOCK_SIZE);

    let mut blocks = Vec::with_capacity(blocks_count);
    for i in 0..blocks_count {
        blocks.push(Block::init_from_buffer(&buf, i));
    }

    for _ in 0..blocks_count {
        if let Some((first_block, other_blocks)) = blocks.split_first_mut() {
            for j in 0..other_blocks.len() {
                let mut other_block = &mut other_blocks[j];
                if !first_block.has_all_neighbours() {
                    first_block.try_fit(&mut other_block);
                }
            }
        }
    }

    let mut first_block = &blocks[0];
    loop {
        match (first_block.neighbour_top, first_block.neighbour_left) {
            (Some(i), _) | (_, Some(i)) => first_block = &blocks[i],
            (None, None) => break,
        }
    }
    loop {
        print!("{} ", first_block.idx);

        let mut next_block = first_block;
        while let Some(i) = next_block.neighbour_right {
            next_block = &blocks[i];
            print!("{} ", next_block.idx);
        }

        match first_block.neighbour_bottom {
            Some(i) => first_block = &blocks[i],
            None => break,
        }
    }
}
